import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch
from sklearn.metrics import roc_auc_score
import scipy.stats as stats

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset as d
from boa_vae import *

# model and output files

def test():
    dataset = d.LatticeDataset(dataset_folder)

    # load model
    data = torch.load(boa_file, map_location='cpu')
    similarity_predictor = data['similarity_predictor']
    att_cointent_slice = data['att_cointent_slice']

    # run test
    records = []
    with torch.no_grad():
        for context, intents, sequence in dataset:
            context = torch.from_numpy(context).float()
            intents = torch.from_numpy(intents).t().float()
            a_embeddings, mu, logvar = data['encoder'](context.view(1, *context.size()))

            cointent = attribute_co_intent_similarity(intents)
            p_cointent = similarity_predictor(a_embeddings[:, :, att_cointent_slice])
            #print(cointent.size(), p_cointent.size())
            for i in range(cointent.size(0)):
                for j in range(cointent.size(0)):
                    if i == j:
                        note = "i = j"
                    elif (intents.t()[i] + intents.t()[j] == 0).all():
                        note = "both empty"
                    else:
                        note = ''
                    records.append({'co-intent similarity': cointent[i,j].item(), 'co-intent similarity prediction': p_cointent[0,i,j].item(), 'note': note, 'i':i, 'j':j})

    # analyse and plot
    df = pd.DataFrame.from_records(records)

    df["i = j"] = df['i'] == df['j']

    df.to_csv(out_csv)
    return df

def plot_stats(df):
    plt.figure(figsize=fig_size)
    df[""] = df["i = j"].apply(lambda x: "$a_i = a_j$" if x else "$a_i \\neq a_j$")
    sns.scatterplot(hue="", x='co-intent similarity', y='co-intent similarity prediction', data=df, alpha = 0.1)

    plt.savefig(out_png)

    print(stats.pearsonr(df['co-intent similarity'], df['co-intent similarity prediction']))

if __name__ == "__main__":
    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    out_png = model_folder + "/metric_cointent.png"
    out_csv = model_folder + "/metric_cointent.csv"
    dataset_folder = "dataset/test/20x20"
    fig_size = (7,4)
    FROM_CSV = False

    df = pd.read_csv(out_csv) if FROM_CSV else test()
    plot_stats(df)