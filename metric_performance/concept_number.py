import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch
from sklearn.metrics import roc_auc_score
import scipy.stats as stats

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset as d
from boa_vae import *

def test():
    dataset = d.LatticeDataset(dataset_folder)

    # load model
    data = torch.load(boa_file, map_location='cpu')
    length_predictor = data['length_predictor']
    sizes_slice = data['sizes_slice']

    # run test
    records = []
    with torch.no_grad():
        for context, intents, sequence in dataset:
            context = torch.from_numpy(context).float()
            a_embeddings, mu, logvar = data['encoder'](context.view(1, *context.size()))
            l = length_predictor(a_embeddings[:, :, sizes_slice])

            records.append({'concept number': intents.shape[1], 'concept number prediction': l.item()})

    # analyse and plot
    df = pd.DataFrame.from_records(records)
    df['distance'] = df['concept number'] - df['concept number prediction']
    df['distance'] = df['distance'].apply(lambda x: max(x, -x))

    df.to_csv(out_csv)
    return df

def plot_stats(df):
    plt.figure(figsize=fig_size)
    sns.scatterplot(x='concept number', y='concept number prediction',data=df,alpha=0.5)
    min_max = [
        min(df['concept number'].min(), df['concept number prediction'].min()),
        max(df['concept number'].max(), df['concept number prediction'].max())
    ]
    plt.plot(min_max, min_max, label='perfect predictor (y=x)', color='orange')
    plt.legend()

    plt.savefig(out_png)

    print(df['distance'].describe())
    print(stats.pearsonr(df['concept number'], df['concept number prediction']))

if __name__ == "__main__":
    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    out_png = model_folder + "/metric_concept_number.png"
    out_csv = model_folder + "/metric_concept_number.csv"
    dataset_folder = "dataset/test/20x20"
    fig_size = (7,4)
    FROM_CSV = False

    df = pd.read_csv(out_csv) if FROM_CSV else test()
    plot_stats(df)