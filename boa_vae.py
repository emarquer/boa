# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Bag of attributes
# 
# positional encoding
# no more rnn pre-encoder: replaced by attribute pre-encoder, negative/positive application, and random key
# alternative

# %%
import numpy as np
import pickle
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from torch import save as torch_save, load as torch_load, Tensor
from pprint import pprint
from dataset import get_dataset, Collate, LatticeDataset, DataAugment
import typing as t
from math import exp
from datetime import datetime
time = lambda: datetime.now().strftime("%H:%M:%S.%f")[:-3]


# %%
class BOA_encoder(nn.Module):
    def __init__(self, emb_size=32, pre_emb_size=4, layers=2, reduction="mean", object_reduction="max"):
        super().__init__()
        self.reduction = reduction
        self.object_reduction = object_reduction
        self.pre_emb_size = pre_emb_size
        self.emb_size = emb_size * 2
        self.layers = layers
        self.object_preencoder = nn.LSTM(1, self.pre_emb_size, num_layers=self.layers, bidirectional=True, batch_first=True)
        self.attribute_encoder_rnn = nn.LSTM(self.pre_emb_size * 2, self.emb_size, num_layers=self.layers, bidirectional=True, batch_first=True)
        self.attribute_encoder_linear = nn.Sequential(
            nn.Linear(2 * (self.emb_size * 2 * self.layers) + self.emb_size, 4 * self.emb_size),
            nn.ReLU(),
            nn.Linear(4 * self.emb_size, 2 * self.emb_size),
            nn.ReLU(),
            nn.Linear(2 * self.emb_size, self.emb_size),
            nn.ReLU(),
            nn.Linear(self.emb_size, self.emb_size//2),#new
            nn.ReLU(),
            nn.Linear(self.emb_size//2, self.emb_size)#new
        )

    def flatten_parameters(self):
        self.object_preencoder.flatten_parameters()
        self.attribute_encoder_rnn.flatten_parameters()

    def forward(self, input):
        #self.flatten_parameters()
        batch, num_o, num_a = input.size()
        # input: [batch x num_o x num_a]
        
        # --- step 1: encode the attributes wrt. other attributes in each object ---
        # input: [batch x num_o x num_a]
        preencoded_o, _ = self.object_preencoder(input.view(batch * num_o, num_a, 1))
        # preencoded_objects: [batch * num_o x num_a x 2 * emb_size]
        if self.reduction == 'max':
            preencoded_o = preencoded_o.view(batch, num_o, num_a, 2, self.pre_emb_size).max(dim=-2)[0]
        else:
            preencoded_o = preencoded_o.view(batch, num_o, num_a, 2, self.pre_emb_size).mean(dim=-2)
        preencoded_o = preencoded_o.view(batch, num_o, num_a, self.pre_emb_size)
        # preencoded_objects: [batch x num_o x num_a x emb_size]
        # boa attribute encoder
        encoded_a_list = []
        for i in range(num_a):
            a = preencoded_o[:, :, i]
            not_a = preencoded_o[:, :, :i].sum(dim=-2) + preencoded_o[:, :, i+1:].sum(dim=-2)
            # a, not_a: [batch x num_o x emb_size]
            o, (h_n, c_n) = self.attribute_encoder_rnn(torch.cat([a, not_a], dim = -1))
            if self.reduction == 'max':
                o_mean = o.view(batch, num_o, 2, self.emb_size).max(dim=-2)[0].max(dim=-2)[0]
            else:
                o_mean = o.view(batch, num_o, 2, self.emb_size).mean(dim=-2).mean(dim=-2)
            h_n = h_n.view(batch, -1)
            c_n = c_n.view(batch, -1)
            # o: [batch x emb_size]
            # h_n, c_n: [batch x 2 * layers * emb_size]
            encoded_a = self.attribute_encoder_linear(torch.cat([h_n, c_n, o_mean], dim=-1))
            # encoded_a: [batch x 2 x emb_size]
            encoded_a_list.append(encoded_a)
        a_embeddings = torch.stack(encoded_a_list, dim=-2)
        # a_embeddings: [batch x num_a x emb_size]

        a_embeddings, mu, logvar = self.reparameterize(a_embeddings)

        return a_embeddings, mu, logvar

    def reparameterize(self, h):
        mu, logvar = h.chunk(2, dim=-1)
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std, mu, logvar

    def encode_objects(self, input, a_embeddings):
        """ encode the objects wrt. their attributes and the embedding """
        batch, num_o, num_a = input.size()
        _, __, emb_size = a_embeddings.size()
        # input: [batch x num_o x num_a]
        
        oa_embeddings = torch.stack([input] * emb_size, dim=-1) * a_embeddings.view(batch, 1, num_a, emb_size)
        # oa_embeddings: [batch x num_o x num_a x emb_size]
        if self.object_reduction == 'max':
            o_embeddings = oa_embeddings.max(dim=-2)[0]
        else:
            o_embeddings = oa_embeddings.mean(dim=-2)
        # o_embeddings: [batch x num_o x emb_size]

        return o_embeddings


# %%
class BOA_decoder(nn.Module):
    def __init__(self, emb_size=16):
        super().__init__()
        self.emb_size = emb_size
        self.oa_decoder = nn.Sequential(
            nn.Linear(self.emb_size * 2, self.emb_size * 2),
            nn.ReLU(),
            nn.Linear(self.emb_size * 2, self.emb_size * 2),
            nn.ReLU(),
            nn.Linear(self.emb_size * 2, self.emb_size),
            nn.ReLU(),
            nn.Linear(self.emb_size, 1),
            nn.Sigmoid()
        )

    def forward(self, a_embeddings, o_embeddings):
        batch, num_a, _ = a_embeddings.size()
        batch, num_o, _ = o_embeddings.size()
        # a_embeddings: [batch x num_a x emb_size]
        # o_embeddings: [batch x num_o x emb_size]
        
        o_embeddings_ = o_embeddings.expand(num_a, batch, num_o, self.emb_size).permute(1, 2, 0, 3)
        a_embeddings_ = a_embeddings.expand(num_o, batch, num_a, self.emb_size).transpose(0, 1)
        # a_embeddings_: [batch x num_o x num_a x emb_size]
        # o_embeddings_: [batch x num_o x num_a x emb_size]
        oa_embeddings = torch.cat([a_embeddings_, o_embeddings_], dim=-1)
        
        pred = self.oa_decoder(oa_embeddings).squeeze(-1)
        # pred: [batch x num_o x num_a]
        return pred

class SimilarityPredictor(nn.Module):
    def __init__(self, emb_size):
        super().__init__()
        self.emb_size = emb_size
        self.predictor = nn.Sequential(
            nn.Linear(self.emb_size * 2, self.emb_size),
            nn.ReLU(),
            nn.Linear(self.emb_size, 1),
            nn.Sigmoid()
        )
    
    def forward(self, a_embeddings):
        batch, num_a, emb_size = a_embeddings.size()
        # a_embeddings: [batch x num_a x emb_size]
        a_embeddings_stacked = a_embeddings.expand(num_a, batch, num_a, emb_size).transpose(0, 1).float() # [batch x num_a x num_a' x emb_size]
        # exchange attribute and stack dimensions
        a_embeddings_stacked_transposed = a_embeddings_stacked.transpose(1, 2) # [batch x num_a' x num_a x emb_size]

        x = torch.cat([a_embeddings_stacked, a_embeddings_stacked_transposed], dim = -1) # [batch x num_a' x num_a x 2 * emb_size]

        pred = self.predictor(x) # [batch x num_a' x num_a x 1]
        pred = pred.view(pred.size()[:-1]) # [batch x num_a' x num_a]

        return pred

class LengthPredictor(nn.Module):
    def __init__(self, emb_size, reduction="mean"):
        super().__init__()
        self.emb_size = emb_size
        self.reduction = reduction
        self.predictor = nn.Sequential(
            nn.Linear(self.emb_size, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            nn.ReLU()
        )
    
    def forward(self, a_embeddings):
        # a_embeddings: [batch x num_a x emb_size]
        if self.reduction == "mean":
            reduced = a_embeddings.mean(dim=-2) # [batch x emb_size]
        else:
            reduced = a_embeddings.max(dim=-2)[0] # [batch x emb_size]
        pred = self.predictor(reduced) # [batch x 1]

        return pred.view(-1)

# %%
def attribute_co_intent_similarity(intents, epsilon=1e-8):
    # co_occurence_matrix[i,j] = number of time i & j appear in the same intent
    co_occurence_matrix = torch.matmul(intents.transpose(-1, -2), intents)
    occurence_vector = intents.sum(dim=-2)
    # appearance_matrix[i,j] = #i + #j + epsilon
    if intents.dim() == 3: # batch variant
        occurence_matrix = occurence_vector.expand(co_occurence_matrix.size(-1), *occurence_vector.size()).transpose(1, 0)
        appearance_matrix = occurence_matrix.transpose(-1, -2) + occurence_matrix + epsilon
    else:
        appearance_matrix = occurence_vector.expand(co_occurence_matrix.size()).transpose(-1, -2) + occurence_vector + epsilon
    # co-occurence similarity between i and j:
    # 2 * #intents containing both i&j / (#intents containing both i + #intents containing both j + epsilon)
    # (2 * co_occurence_matrix[i,j].float()) / (intents[:,i].sum() + intents[:,j].sum() + epsilon).float()
    co_occurence_similarity_matrix = 2 * co_occurence_matrix.float() / appearance_matrix
    # if the sum of the individual occurences are 0, it means that both attributes are empty, and thus a particular case of similarity
    co_occurence_similarity_matrix += (appearance_matrix <= epsilon).float() 
    return co_occurence_similarity_matrix
def attribute_co_intent_distance(intents, epsilon=1e-8):
    # co_occurence_matrix[i,j] = number of time i appears without j in the same intent
    no_occurence_matrix = torch.matmul(intents.transpose(-1, -2), 1 - intents)
    occurence_vector = intents.sum(dim=-2)
    # appearance_matrix[i,j] = #i + #j + epsilon
    if intents.dim() == 3: # batch variant
        occurence_matrix = occurence_vector.expand(no_occurence_matrix.size(-1), *occurence_vector.size()).transpose(1, 0)
        appearance_matrix = occurence_matrix.transpose(-1, -2) + occurence_matrix + epsilon
    else:
        appearance_matrix = occurence_vector.expand(no_occurence_matrix.size()).transpose(-1, -2) + occurence_vector + epsilon
    # co-occurence distance between i and j:
    # #intents containing i but not j + #intents containing j but not i / (#intents containing both i + #intents containing both j + epsilon)
    # (2 * co_occurence_matrix[i,j].float()) / (intents[:,i].sum() + intents[:,j].sum() + epsilon).float()
    co_occurence_distance_matrix = (no_occurence_matrix + no_occurence_matrix.transpose(-1, -2)) / appearance_matrix
    return co_occurence_distance_matrix   
def attribute_cosine_similarity(a_embeddings):
    source = a_embeddings.expand(a_embeddings.size(-2), *a_embeddings.size()).float()
    target = source.transpose(0, -2) # exchange attribute and duplicate dimensions
    pairwise_cosine_similarity = torch.cosine_similarity(source, target, dim=-1)
    if a_embeddings.dim() == 3: # batch variant
        pairwise_cosine_similarity = pairwise_cosine_similarity.transpose(0, 1)
    return pairwise_cosine_similarity
