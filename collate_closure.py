from dataset import *
import torch

def attribute_set_inclusion(x, refference):
    """Check if a set defined by a boolean or binary tensor is in the refference set."""
    return (x <= refference).all().bool()

def close(x, intents):
    """Assume the intents are in the level order."""
    for intent in intents:
        if attribute_set_inclusion(x, intent):
            return intent

class CollateClosure(Collate):
    """ Collate object for pre-processing the loaded data. """

    def __call__(self, batch):
        """ Pre-processes a batch of loaded data.

        :param batch:
            The batch of data to preprocess.
        
        :return:
            The preprocessed batch of data, as a tuple.
            In order:
            :contexts:
                The contexts matrix, of size (batch, num_o, num_a).
            :intents:
                The intents matrix, of size (batch, num_c, num_a).
            :stops:
                The stops matrix, of size (batch, num_c).
                The elements are 0 if the corresponding concept is not the last and 1 if it is.
            :links:
                The flattened adjacency matrix, of size (batch, num_c * num_c / 2). If transitive_links==True in the constructor, the size is (batch, num_c * num_c / 2, 2).
            :sizes:
                The number of concepts vector, of size (batch).
            :sets_to_close:
                TODO
            :sets_closure:
                TODO
            :c_mask:
                Only present if masks==True in the constructor.
                A boolean mask with False where the contexts matrix corresponds to padding and True otherwise.
            :i_mask:
                Only present if masks==True in the constructor.
                A boolean mask with False where the intents matrix corresponds to padding and True otherwise.
            :l_mask:
                Only present if masks==True in the constructor.
                A boolean mask with False where the flattened adjacency matrix corresponds to padding and True otherwise.
        """
        if self.masks:
            contexts, intents, stops, links, sizes, c_mask, i_mask, l_mask = super(CollateClosure, self).__call__(batch)
        else:
            contexts, intents, stops, links, sizes = super(CollateClosure, self).__call__(batch)
            
        batch_size, intent_number, attribute_number = intents.size()
        set_number = contexts.size(1)#intent_number
        available_attributes = intents.bool().any(dim=-2)
        sets_to_close = torch.rand(intents.size()).to(intents.device) < 0.3
        # remove
        sets_to_close = sets_to_close * available_attributes.view(batch_size, 1, attribute_number)
        
        sets_closure = torch.zeros_like(sets_to_close)
        for batch in range(batch_size):
            for set_id in range(set_number):
                sets_closure[batch, set_id] = close(sets_to_close[batch, set_id], intents[batch])
                
        if self.masks:
            return contexts, intents, stops, links, sizes, sets_to_close, sets_closure, c_mask, i_mask, l_mask
        else:
            return contexts, intents, stops, links, sizes, sets_to_close, sets_closure 
