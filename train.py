import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from dataset import get_dataset, Collate, LatticeDataset, DataAugment
from math import exp
from cointent import attribute_co_intent_similarity
from kl_divergence import kl_divergence
from boa_vae import BOA_encoder, BOA_decoder, SimilarityPredictor, LengthPredictor

from datetime import datetime
time = lambda: datetime.now().strftime("%H:%M:%S.%f")[:-3]


if __name__=="__main__":
    # metric learning mode if an argument is provided, pretraining otherwise
    import sys
    mode_metric_learn = len(sys.argv) > 1 

    # hyperparameters
    name = "boa_vae"
    device = 'cuda'
    batch_size = 8
    shuffle = True
    num_workers = 8
    lr = 1e-3
    weight_decay = 0
    epochs = 500
    # embedding dimensions
    pre_emb_size = 64
    emb_size = 124
    att_cointent_slice = slice(0, emb_size//2)
    sizes_slice = slice(emb_size//2, emb_size//2 + emb_size//4)
        # metric loss weight
    metric_weight = lambda epoch_: min(epoch_ / (epochs / 2), 1)
    # kl divergence weight
    beta = 1e-3
    # kl annealing = 1 / (1 + exp(slope * (-x + center)))
    slope = 2
    center = 50
    annealing = lambda epoch_: 1. / (1. + exp(slope * (-epoch_ + center)))
    
    
    # load the dataset
    train_dataset, dev_dataset = get_dataset("dataset/train") # load and split train into 90% train and 10% dev
    eval_dataset = LatticeDataset("dataset/test")
    
    # build the data preprocessing and data augmentation processes
    if not mode_metric_learn:
        collate = Collate(DataAugment(0.1, 0.01))
    else:
        collate = Collate(DataAugment(0.1))

    # create dataloader
    train_dataloader = DataLoader(train_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    dev_dataloader = DataLoader(dev_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    eval_dataloader = DataLoader(eval_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)

    # create or load model
    if not mode_metric_learn:
        # create a new BoA model
        encoder = BOA_encoder(emb_size=emb_size, pre_emb_size=pre_emb_size).to(device)
        decoder = BOA_decoder(emb_size=emb_size).to(device)
        data = {"encoder": encoder, "decoder": decoder}
    else:
        # load pre-trained model
        data = torch.load(name + '.tch', map_location=device)
        encoder = data["encoder"]
        decoder = data["decoder"]

        # create metric predictors
        similarity_predictor = SimilarityPredictor(emb_size=emb_size//2).to(device)
        length_predictor = LengthPredictor(emb_size=emb_size//4).to(device)
        data["similarity_predictor"] = similarity_predictor
        data["length_predictor"] = length_predictor
        data["att_cointent_slice"] = att_cointent_slice
        data["sizes_slice"] = sizes_slice
        
        # extend the name to avoid overwriting the model
        name += '_metric'

    print(f"{time()} starting metric learning" if mode_metric_learn else f"{time()} initial training")
    
    # a list of dictionaries containing information about the training and development phases
    # meant to be loaded with Pandas and plotted with Seaborn
    logs = []

    # criterions, optimizers and learning rate scheduler
    bce_criterion = nn.BCELoss()
    mse_criterion = nn.MSELoss()
    optim = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()], lr, weight_decay=weight_decay)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optim, patience=20, verbose=True, eps=1e-7, factor=0.1, cooldown=20)

    if mode_metric_learn:
        optim_metric_similarity = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in similarity_predictor.parameters()], lr)
        optim_metric_length = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in length_predictor.parameters()], lr)
    
    def dev_performance(epoch):
        """ Computes the performance on the development dataset. """
        with torch.no_grad():
            losses = []
            kls = []
            att_cointent_losses = []
            sizes_losses = []
            for contexts, intents, stops, links, sizes in dev_dataloader:
                contexts = contexts.to(device) # [batch x num_o x num_a]
                intents = intents.to(device)
                sizes = sizes.to(device)

                # encode and decode contexts
                a_emb, mu, logvar = encoder(contexts)
                o_emb = encoder.encode_objects(contexts, a_emb)
                pred = decoder(a_emb, o_emb)

                # compute binary cross-entropy and KL divergence
                loss = bce_criterion(pred, contexts.float())
                losses.append(loss.detach().cpu().item())
                kl = kl_divergence(mu, logvar)
                kls.append(kl.detach().cpu().item())
                loss += kl * beta * annealing(epoch)

                # compute metric losses
                if mode_metric_learn:
                    att_cointent_loss = mse_criterion(similarity_predictor(a_emb[:, :, att_cointent_slice]), attribute_co_intent_similarity(intents))
                    att_cointent_losses.append(att_cointent_loss.detach().cpu().item())
                    loss += att_cointent_loss * metric_weight(epoch)
                
                    sizes_loss = mse_criterion(length_predictor(a_emb[:, :, sizes_slice]), sizes.float())
                    sizes_losses.append(sizes_loss.detach().cpu().item())
                    loss += sizes_loss * metric_weight(epoch)
                    
            
            log = {"set": "dev", "epoch": epoch}
            log["reconstruction loss"] = sum(losses)/len(losses)
            log["kl divergence"] = sum(kls)/len(kls)
            log["kl annealing"] = annealing(epoch)
            if not mode_metric_learn:
                print(f"{time()} dev: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:9.5f} x {log['kl annealing']:.3f}")
            else:
                log["metric weight"] = metric_weight(epoch)

                # reconstruction performance
                message = f"{time()} dev: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:15.5f} x {log['kl annealing']:.3f}"
                
                # co-intent prediction performance
                log["co-intent metric loss"] = sum(att_cointent_losses)/len(att_cointent_losses)
                message += f", co-intent loss: {log['co-intent metric loss']:11.5f} x {log['metric weight']:.3f}"
                
                # number of concept prediction performance
                log["number of concept metric loss"] = sum(sizes_losses)/len(sizes_losses)
                message += f", number of concept loss: {log['number of concept metric loss']:11.5f} x {log['metric weight']:.3f}"
                    
                print(message)
            return log
    
    # training loop
    for epoch in range(epochs):
        losses = []
        kls = []
        att_cointent_losses = []
        sizes_losses = []
        for contexts, intents, stops, links, sizes in train_dataloader:
            contexts = contexts.to(device)
            intents = intents.to(device)
            sizes = sizes.to(device)

            # encode and decode contexts
            a_emb, mu, logvar = encoder(contexts)
            o_emb = encoder.encode_objects(contexts, a_emb)
            pred = decoder(a_emb, o_emb)

            # compute binary cross-entropy and KL divergence
            loss = bce_criterion(pred, contexts.float())
            losses.append(loss.detach().cpu().item())
            kl = beta * kl_divergence(mu, logvar)
            kls.append(kl.detach().cpu().item())
            loss += kl * beta * annealing(epoch)

            # compute metric losses and optimize
            # metric learning alternates between co-intent similarity prediction and number of concepts prediction
            if mode_metric_learn:
                if epoch % 2 == 0:
                    att_cointent_loss = mse_criterion(similarity_predictor(a_emb[:, :, att_cointent_slice]), attribute_co_intent_similarity(intents))
                    att_cointent_losses.append(att_cointent_loss.detach().cpu().item())
                    loss += att_cointent_loss * metric_weight(epoch)
                    optim_metric_similarity.zero_grad()
                    loss.backward()
                    optim_metric_similarity.step()
                else:
                    sizes_loss = mse_criterion(length_predictor(a_emb[:, :, sizes_slice]), sizes.float())
                    sizes_losses.append(sizes_loss.detach().cpu().item())
                    loss += sizes_loss * metric_weight(epoch)
                    optim_metric_length.zero_grad()
                    loss.backward()
                    optim_metric_length.step()
            else:
                optim.zero_grad()
                loss.backward()
                optim.step()

        # run the scheduler only after the annealing is in place
        if epoch > center:
            scheduler.step(sum(losses)/len(losses) + sum(kls)/len(kls))

        # print performance
        log = {"set": "train", "epoch": epoch}
        log["reconstruction loss"] = sum(losses)/len(losses)
        log["kl divergence"] = sum(kls)/len(kls)
        log["kl annealing"] = annealing(epoch)
        if not mode_metric_learn:
            print(f"{time()} {epoch:3}: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:9.5f} x {log['kl annealing']:.3f}")
        else:
            log["metric weight"] = metric_weight(epoch)

            # reconstruction performance
            message = f"{time()} {epoch:3}: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:15.5f} x {log['kl annealing']:.3f}"
            
            # co-intent prediction performance
            message += ", co-intent loss: "
            log["co-intent metric loss"] = sum(att_cointent_losses)/len(att_cointent_losses) if len(att_cointent_losses) > 0 else 'NaN'
            if log["co-intent metric loss"] != 'NaN':
                message += f"{log['co-intent metric loss']:11.5f} x {log['metric weight']:4.3f}"
            else:
                message += " "*18
            
            # number of concept prediction performance
            message += ", number of concept loss: "
            log["number of concept metric loss"] = sum(sizes_losses)/len(sizes_losses) if len(sizes_losses) > 0 else 'NaN'
            if log["number of concept metric loss"] != 'NaN':
                message += f"{log['number of concept metric loss']:11.5f} x {log['metric weight']:4.3f}"
            else:
                message += " "*18
                
            print(message)
                
        # store logs and save model
        logs.append(log)
        logs.append(dev_performance(epoch))
        data["epoch logs"] = logs
        torch.save(data, name + '.tch')






    # examine reconstruction performance on the validation set, by computing the AUC ROC and plotting side by side
    # predictions and true contexts
    accuracies, row_accuracy, cell_accuracy = [], [], []
    aucs = []
    from sklearn.metrics import roc_auc_score
    with torch.no_grad():
        for j in range(1,10):
            print(f"--- {j/10} ---")
            for contexts, intents, stops, links, sizes in eval_dataloader:
                a_emb, mu, logvar = encoder.cpu()(contexts)
                o_emb = encoder.encode_objects(contexts, mu)
                pred = decoder.cpu()(mu, o_emb)

                for i in range(len(pred)):
                    try:
                        auc = roc_auc_score(contexts[i].view(-1), pred[i].view(-1))
                        aucs.append(auc)
                    except ValueError:
                        pass

                accuracy = (contexts > 0) == (pred > j/10) # compare the cells
                cell_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)
                row_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)  # check for each sample in the batch if all its cells wwere correctly predicted
                accuracies.append(accuracy.cpu())
            print(f"accuracy (rate of contexts sets fully well-predicted): {torch.cat(accuracies, dim=0).type(torch.float).mean()}")
            print(f"row_accuracy: {torch.cat(row_accuracy).type(torch.float).mean()}")
            print(f"cell_accuracy: {torch.cat(cell_accuracy).type(torch.float).mean()}")
            print(f"AUC ROC average: {sum(aucs)/len(aucs):.5f}")
            break


        # plot 
        import matplotlib.pyplot as plt
        for contexts, intents, stops, links, sizes in eval_dataloader:
            a_emb, mu, logvar = encoder.cpu()(contexts)
            o_emb = encoder.encode_objects(contexts, mu)
            pred = decoder.cpu()(mu, o_emb)

            f = plt.figure(figsize=(10, len(pred) * 2))
            for i, (prediction, sample) in enumerate(zip(pred.cpu(), contexts)):
                f.add_subplot(3, len(pred), i+1)
                plt.imshow(prediction.detach().cpu().numpy(), vmin=0, vmax=1)

                f.add_subplot(3, len(pred), i+len(pred)+len(pred)+1)
                plt.imshow(sample.cpu().numpy(), vmin=0, vmax=1)
            plt.savefig(name + '.png')
            break
