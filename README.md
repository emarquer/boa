# Bag of Attributes: Embedding Formal Contexts Using Unordered Composition
## About
This repository contains the code of Bag of Attribute (BoA) proposed in the article [Embedding Formal Contexts Using Unordered Composition](https://hal.archives-ouvertes.fr/hal-02912874).

It is the result of the Master 2 internship work of Esteban Marquer, under the supervision of Miguel Couciero and Ajinkya Kulkarni, with the help of Alain Gely and Alexandre Bazin.

Our experiments were carried out using the Grid'5000 testbed, supported by a scientific interest group hosted by Inria and including CNRS, RENATER and several Universities as well as other organizations (see https://www.grid5000.fr).

We would like to thank the Inria Project Lab (IPL) HyAIAI ("Hybrid Approaches for Interpretable Artificial Intelligence") for funding this project.

## Cite this repository
```bib
@inproceedings{boa:2020:marquer,
  TITLE = {{Embedding Formal Contexts Using Unordered Composition}},
  AUTHOR = {Marquer, Esteban and Kulkarni, Ajinkya and Couceiro, Miguel},
  URL = {https://hal.archives-ouvertes.fr/hal-02912874},
  BOOKTITLE = {{FCA4AI - 8th International Workshop ''What can FCA do for Artificial Intelligence?''  (colocated with ECAI2020)}},
  ADDRESS = {Santiago de Compostela, Spain},
  YEAR = {2020},
  MONTH = Aug,
  KEYWORDS = {Formal Concept Analysis ; Vector Space Embedding ; Neural Networks ; Complex Data ; Link Prediction ; Clustering},
  PDF = {https://hal.archives-ouvertes.fr/hal-02912874/file/BoA_article_FCA4AI%20%283%29.pdf},
  HAL_ID = {hal-02912874},
  HAL_VERSION = {v1},
}
```

## Outline of this file
- [Bag of Attributes: Embedding Formal Contexts Using Unordered Composition](#bag-of-attributes-embedding-formal-contexts-using-unordered-composition)
  - [About](#about)
  - [Cite this repository](#cite-this-repository)
  - [Outline of this file](#outline-of-this-file)
  - [Requirements](#requirements)
    - [Anaconda or Miniconda](#anaconda-or-miniconda)
    - [Virtualenv, venv or pip](#virtualenv-venv-or-pip)
  - [How to use](#how-to-use)

## Requirements
The code present in this repository is designed for Python 3.8 and is based on the PyTorch deep-learning library.
We recommend the use of Anaconda to set up the work environement.
The following two sections contain the required code to install the dependencies respectively with Anaconda and pip.

### Anaconda or Miniconda
```bash
conda create -y --name boa python==3.8
conda install -y --name boa -c conda-forge -c pytorch --file requirements.txt
conda activate boa
```
### Virtualenv, venv or pip
```bash
pip install -r requirements_pip.txt
```

## How to use
Once the dependencies are set up, run `python train.py` to train a BoA model without metric learning.
You can then train the model with metric learning, by running `python train.py -m` (in practice, the presence of any argument will trigger metric learning).

The resulting model will be saved as `boa_vae.tch` and `boa_vae_metric.tch`.
This file is a PyTorch pickle.

To load a model, use for example `data = torch.load('boa_vae_metric.tch')` after importing `boa_vae.py`.
You can then access the various components of the file from the data dictionary, such as `data['encoder'](my_context)`.
The keys are:

- `encoder`: the BoA encoder;
- `decoder`: the BoA decoder;
- `epoch logs`: a list of dictionary summarizing the training process performance.

Additionally, after running the metric learning phase, additional keys are available:

- `similarity_predictor`: the co-intent pairwise similarity matrix predictor;
- `length_predictor`: the number of concepts predictor;
- `att_cointent_slice`: a slice corresponding to the portion od the attribute embeddings used to predict the co-intent pairwise similarity matrix;
- `sizes_slice`: a slice corresponding to the portion od the attribute embeddings used to predict the number of concepts.

### Evaluation of performance and tasks
The evaluation of the limits of BoA are:
- : `limits/attribute_clustering.py`;
- : `limits/attribute_clustering.py`;

Run `python tasks/<task_name>.py` to run a task, for example with the attribute clustering task: `python tasks/attribute_clustering.py`.

The evaluation tasks available for the evaluation of BoA are:
- attribute clustering task on SPECT: `tasks/attribute_clustering.py`;
- link prediction task in ICFCA: `tasks/attribute_clustering.py`, has issues with the meaningfulness of the results, see the "Embedding Formal Contexts Using Unordered Composition" article.

Run `python tasks/<task_name>.py` to run a task, for example with the attribute clustering task: `python tasks/attribute_clustering.py`.

You can then train the model with metric learning, by running `python train.py -m` (in practice, the presence of any argument will trigger metric learning).