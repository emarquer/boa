import torch, torch.nn as nn

cosine_distance = nn.CosineSimilarity(dim=-1)
euclidean_distance = lambda x, y: torch.norm(x - y, dim=-1)
hamming_distance = lambda x, y: (x != y).sum(dim=-1)

class Closure2Vec(nn.Module):
    def __init__(self, attribute_set_length, object_set_length, closure_size, use_euclidean_distance=True):
        """Closure2Vec architecture from the FCA2VEC article (Dominik Dürrschnabel, Tom Hanika, and Maximilian 
        Stubbemann).
        
        We reffer to the notations used in the article to designate the layers.
        M: attribute set
        G: object set
        """
        super().__init__()
        self.input_size = attribute_set_length #|M|
        self.hidden_size = object_set_length   #|G|
        self.closure_size = closure_size       #2 or 3 dimensions (l 2 p 10)
        self.phi = nn.Sequential(nn.Linear(self.input_size, self.hidden_size), nn.ReLU())  #φ
        self.psi = nn.Sequential(nn.Linear(self.hidden_size, self.input_size), nn.ReLU())  #ψ
        self.roh = nn.Sequential(nn.Linear(self.input_size, self.closure_size), nn.ReLU()) #ρ
        self.delta = euclidean_distance if use_euclidean_distance else cosine_distance     #δ
        
    def forward(self, i, i_prime):
        input = torch.stack((i, i_prime))

        out = self.roh(self.psi(self.phi(input)))

        distance = self.delta(*out)

        return distance


if __name__ == "__main__":
    import random

    lr = 1e-4
    epochs = 1000
    batch_number = 100
    batch_length = 8
    attribute_set_length = 20
    object_set_length = 50
    closure_size = 2

    attribute_set = [i for i in range(attribute_set_length)]
    batchs = [
        ((torch.randn(batch_length, attribute_set_length) > 0).type(torch.long),
         (torch.randn(batch_length, attribute_set_length) > 0).type(torch.long)) for i in range(batch_number)
    ]

    model = Closure2Vec(attribute_set_length, object_set_length, closure_size)
    optim = torch.optim.Adam(model.parameters(), lr = lr)
    criterion = nn.MSELoss()

    for epoch in range(epochs):
        losses = []
        for attribute_set_1, attribute_set_2 in batchs:
            attribute_set_1, attribute_set_2 = attribute_set_1.type(torch.float), attribute_set_2.type(torch.float)

            out = model(attribute_set_1, attribute_set_2)
        
            target = hamming_distance(attribute_set_1, attribute_set_2)
            loss = criterion(out, target.type(torch.float))

            loss.backward()
            optim.step()
            optim.zero_grad()

            losses.append(loss.cpu().item())
        
        if epoch % 10 == 0: print(sum(losses)/len(losses))
