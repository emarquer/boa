# %%
import torch, torch.nn as nn
import random

# %%
def to_samples_one_hot(list_of_extents, vocabulary_length, mode):
    # version from the article
    samples = []
    φ = lambda x: torch.nn.functional.one_hot(torch.tensor(x), vocabulary_length) # φ

    random.shuffle(list_of_extents)
    for extent in list_of_extents:
        random.shuffle(extent)
        for object_ in extent:
            extent_without_object = [other_object for other_object in extent if other_object != object_]

            if mode == "SG":
                for other_object in extent_without_object:
                    samples.append((φ(object_), φ(other_object)))
                        
            elif mode == "CBoW" and len(extent_without_object) > 0:
                average = sum(φ(other_object) for other_object in extent_without_object).type(torch.float)
                average /= len(extent_without_object)

                samples.append((average, φ(object_)))
    return samples

def to_samples(list_of_extents, mode):
    # version without the one-hot, simpler for pytorch
    samples = []

    random.shuffle(list_of_extents)
    for extent in list_of_extents:
        random.shuffle(extent)
        for object_ in extent:
            extent_without_object = [other_object for other_object in extent if other_object != object_]

            if mode == "SG":
                for other_object in extent_without_object:
                    samples.append((torch.tensor([object_], dtype=torch.long), torch.tensor([other_object], dtype=torch.long)))
                        
            elif mode == "CBoW" and len(extent_without_object) > 0:
                samples.append((torch.tensor(extent_without_object, dtype=torch.long), torch.tensor([object_], dtype=torch.long)))
    return samples

def Object2Vec(embedding_size, vocab_size, mode):
    if mode == "SG":
        return Object2VecSG(embedding_size, vocab_size)
    elif mode == "CBoW":
        return Object2VecCBoW(embedding_size, vocab_size)

class Object2VecSG(nn.Module):
    # from https://rguigoures.github.io/word2vec_pytorch/

    def __init__(self, embedding_size, vocab_size):
        super(Object2VecSG, self).__init__()
        self.embeddings = nn.Embedding(vocab_size, embedding_size)
        self.linear = nn.Linear(embedding_size, vocab_size)
        
    def forward(self, context_word):
        emb = self.embeddings(context_word)
        hidden = self.linear(emb)
        out = nn.functional.log_softmax(hidden)
        return out

    def embed(self, context):
        emb = self.embeddings(context)
        return emb

class Object2VecCBoW(nn.Module):
    # from https://rguigoures.github.io/word2vec_pytorch/

    def __init__(self, embedding_size, vocab_size):
        super(Object2VecCBoW, self).__init__()
        self.embeddings = nn.EmbeddingBag(vocab_size, embedding_size)
        self.linear = nn.Linear(embedding_size, vocab_size)
        
    def forward(self, context_word, offsets=None):
        emb = self.embeddings(context_word, offsets)
        hidden = self.linear(emb)
        out = nn.functional.log_softmax(hidden)
        return out

    def embed(self, context, offsets=None):
        emb = self.embeddings(context, offsets)
        return emb

"""
L ← []
Lext < − list-of-extents(G,M,I) in randomized order (excluding G).
forall A in Lext do
    LA ← list(A), with randomized order.
    forall o in LA do
        if type = SG then
            forall ǒ in LA: do
                if o 6= ǒ then
                    add ((φ(o), φ(ǒ))) to L
        if type = CBoW and |A| > 1 then
            add (( 1 P
            φ(ǒ), φ(o))) to L
            |LA |−1 ǒ∈la,o6=ǒ
return L

"""


if __name__ == "__main__":
    from pprint import PrettyPrinter
    pprint = PrettyPrinter(width=200).pprint

    sample_number = 25
    vocabulary_length = 10
    vocabulary = [i for i in range(vocabulary_length)]
    extents = [
        random.sample(vocabulary, random.randint(0, vocabulary_length))
        for i in range(sample_number)
    ]
    
    """
    pprint(extents)
    input("\npress to continue")
    print()
    pprint(to_samples(extents, vocabulary_length, "SG"))
    input("\npress to continue")
    print()
    pprint(to_samples(extents, vocabulary_length, "CBoW"))
    """

    lr = 1e-4
    epochs = 1000
    batch_length = 8
    embedding_size = 3 #2 or 3 dimensions (l 9 p 15)
    mode = "SG"

    model = Object2Vec(embedding_size, vocabulary_length, mode)
    optim = torch.optim.Adam(model.parameters(), lr = lr)
    criterion = nn.CrossEntropyLoss()

    data = to_samples(extents, mode)
    batchify = lambda x: (torch.stack(y, dim=0) for y in zip(*x))
    batches = [batchify(data[i:i+batch_length]) for i in range(0, len(data), batch_length)]

    for epoch in range(epochs):
        losses = []
        for input, target in data:
            out = model(input)
        
            loss = criterion(out, target)

            loss.backward()
            optim.step()
            optim.zero_grad()

            losses.append(loss.cpu().item())
        
        if epoch % 10 == 0: print(epoch, sum(losses)/len(losses))
# %%