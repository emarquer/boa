import torch
from boa_vae import *
data = torch.load("../concept_models/boa/boa_vae_e500_lr3schedule_b0001_cointent.tch")
with torch.no_grad():
    context_boa = torch.load('object_task_context_boa.tch')
    a_embeddings, mu, logvar = data['encoder'].cpu()(context_boa.view(1, *context_boa.size()))
    torch.save(mu, 'object_task_a_embeddings.tch')

    o_embeddings = data['encoder'].cpu().encode_objects(context_boa.view(1, *context_boa.size()), mu)
    torch.save(o_embeddings, 'object_task_o_embeddings.tch')