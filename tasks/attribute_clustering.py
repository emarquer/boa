from sklearn.cluster import KMeans
import numpy as np
import random, torch
import pickle
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from torch.optim.lr_scheduler import LambdaLR
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from scipy.stats import ttest_ind, ttest_1samp

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset
import fca2vec.object2vec as o2v
from boa_vae import *
#from boa_v0 import *

# load implications
#boa_file = "models/boa.tch"
#boa_file = "models/boa_retrain_cointent.tch"
boa_file = "models/boa_retrain.tch"
#boa_file = "models/boa_v0_c.tch"
a2v_temp_file = "tasks/attribute_clustering_a2v.tch"
data_file = "tasks/SPECT/preprocessed/spect.pkl"
implication_file = "tasks/SPECT/preprocessed/spect.canonical.pkl"
results_csv = "tasks/attribute_clustering_results.csv"
results_png = "tasks/attribute_clustering_results.png"
independance_png = "tasks/attribute_clustering_independance_{}.png"
device = 'cuda' if torch.cuda.is_available() else 'cpu'

# get all A→B ∈ L, as pairs of frozensets of attribute index
with open(implication_file, 'rb') as f:
    implications = pickle.load(f)
    print(list(implications)[2])

order, sequence, object_to_concept, attribute_to_concept, extents, intents, context = dataset.load_lattice(data_file)
intents = intents.transpose()
# extents.shape, context.shape, intents.shape, attribute_to_concept.shape

def train_a2v():
    """Apply Attribute2vec model on data.
    
    It is the same as o2v, with a transpose of the context and using itents instead of extents.
    """
    vocabulary_length = attribute_to_concept.shape[0]
    vocabulary = [i for i in range(vocabulary_length)]
    batch_length = 8
    intents_ = [[i for i, value in enumerate(intent) if value] for intent in intents]

    # CBoW data
    data = o2v.to_samples(intents_, "CBoW")
    batches_CBoW = []
    for batch in (data[i:i+batch_length] for i in range(0, len(data), batch_length)):
        inputs, targets = [], []
        offsets = [0]
        for input, target in batch:
            inputs.append(input)
            targets.append(target)
            offsets.append(offsets[-1] + len(input))
        batches_CBoW.append((torch.cat(inputs), torch.tensor(offsets[:-1]), torch.cat(targets),))

    # Skip-Gram data
    data = o2v.to_samples(intents_, "SG")
    batches_SG = []
    for batch in (data[i:i+batch_length] for i in range(0, len(data), batch_length)):
        inputs, targets = [], []
        for input, target in batch:
            inputs.append(input)
            targets.append(target)
        batches_SG.append((torch.cat(inputs), torch.cat(targets),))

    # Model training
    epochs = 10
    embedding_size = 3 #2 or 3 dimensions (l 9 p 15)
    num_iter = 20
    lr = 1
    lambda_ = lambda epoch: lr - (epoch / epochs)

    emb_models_list = []
    for iteration in range(num_iter):
        for mode in ["SG", "CBoW"]:
            print(iteration, mode)
            model = o2v.Object2Vec(embedding_size, vocabulary_length, mode)
            optim = o2v.torch.optim.Adam(model.parameters(), lr = lr)
            criterion = o2v.nn.CrossEntropyLoss()
            scheduler = LambdaLR(optim, lr_lambda=lambda_)
            if mode == "CBoW":
                for epoch in range(epochs):
                    losses = []
                    for input, offsets, target in batches_CBoW:
                        #if mode == "CBoW": input = input.view(1,-1) # fake batch
                        out = model(input, offsets)

                        loss = criterion(out, target)

                        loss.backward()
                        optim.step()
                        optim.zero_grad()

                        losses.append(loss.cpu().item())
                    
                    print(epoch, sum(losses)/len(losses))
                    scheduler.step()

                with torch.no_grad():
                    attributes = torch.arange(vocabulary_length).view(-1, 1)
                    a2v_emb = model.embeddings(attributes)
            else:
                for epoch in range(epochs):
                    losses = []
                    for input, target in batches_SG:
                        out = model(input)

                        loss = criterion(out, target)

                        loss.backward()
                        optim.step()
                        optim.zero_grad()

                        losses.append(loss.cpu().item())
                    
                    print(epoch, sum(losses)/len(losses))
                    scheduler.step()

                with torch.no_grad():
                    attributes = torch.arange(vocabulary_length)
                    a2v_emb = model.embeddings(attributes)

            emb_models_list.append((a2v_emb, 'a2v ' + mode, ))

    # Model storage
    torch.save(emb_models_list, a2v_temp_file)


def apply_boa():
    """Apply Bag of Attributes model on data."""
    data = torch.load(boa_file, map_location=device)
    with torch.no_grad():
        context_boa = torch.from_numpy(context).float()
        a_embeddings, mu, logvar = data['encoder'].cpu()(context_boa.view(1, *context_boa.size()))

    emb_models_list = torch.load(a2v_temp_file)
    emb_models_list.append((mu[0].numpy(), 'BoA'))

    a_embeddings_reduced = TSNE(random_state=0, n_components=3).fit_transform(mu[0])
    emb_models_list.append((torch.from_numpy(a_embeddings_reduced), 'BoA TSNE 3d'))
    a_embeddings_reduced = PCA(random_state=0, n_components=3).fit_transform(mu[0])
    emb_models_list.append((torch.from_numpy(a_embeddings_reduced), 'BoA PCA 3d'))

    return emb_models_list

def cluster_attributes(emb_models_list):
    results = []
    for instance in range(1):
        for emb, model_name in emb_models_list:
            # k-means clustering
            for k in [2,5,10]:
                kmeans = KMeans(n_clusters=k,random_state=3, n_init = 100).fit(emb)

                # transform clustering in sets
                clusters = {i: set() for i in range(k)}
                for i, label in enumerate(kmeans.labels_):
                    clusters[label].add(i)

                # intra cluster implications: c∈C such that A∪B⊆c
                intra = 0
                for implication in implications:
                    implication_set = implication[0].union(implication[0])
                    for cluster in clusters.values():
                        if implication_set <= cluster:
                            intra += 1
                            break
                
                intra_ratio = intra / len(implications)
                results.append({"model": model_name, "intra-cluster implications": intra_ratio, "k": k})

    df = pd.DataFrame.from_records(results)
    df.to_csv(results_csv)

    return df

def plot(df):
    sns.barplot(data=df, hue='model', y = 'intra-cluster implications', x='k', ci="sd",
        hue_order=['a2v SG', 'a2v CBoW', 'BoA', 'BoA TSNE 3d', 'BoA PCA 3d'])
    plt.savefig(results_png)

def stats(df):
    stats = {}
    for k in [2,5,10]:
        mean = df[df['k']==k].groupby('model')['intra-cluster implications'].mean()
        sd = df[df['k']==k].groupby('model')['intra-cluster implications'].std()
        stats[k] = pd.concat([mean, sd], axis=1, keys=['mean', 'std'])
    stats = pd.concat([stats[2], stats[5], stats[10]], axis=1, keys=['k = 2','k = 5','k = 10'])
    print(stats.to_markdown())

def independence_tests(df):
    for k in [2,5,10]:
        df_ = df[df['k'] == k]
        #models = ['a2v SG', 'a2v CBoW', 'boa', 'boa TSNE 3d', 'boa PCA 3d', 'boa metric', 'boa metric TSNE 3d', 'boa metric PCA 3d']
        models = ['a2v SG', 'a2v CBoW', 'BoA metric', 'BoA metric TSNE 3d', 'BoA metric PCA 3d']
        ps = np.ndarray((len(models), len(models),))
        for i, first_model in enumerate(models):
            for j, second_model in enumerate(models):
                if i != j:
                    first_df = df_[df_['model'] == first_model]['intra-cluster implications']
                    second_df = df_[df_['model'] == second_model]['intra-cluster implications']
                    if len(first_df) > 1:
                        if len(second_df) > 1:
                            p = ttest_ind(first_df, second_df)[1]
                        else:
                            p = ttest_1samp(first_df, second_df)[1]
                    else:
                        if len(second_df) > 1:
                            p = ttest_1samp(second_df, first_df)[1]
                        else:
                            p = 'nan'
                else:
                    p = 'nan'
                ps[i, j] = p


        #fig, ax = plt.subplots(figsize=(10,10))
        fig, ax = plt.subplots(figsize=(6,6))
        im = ax.imshow(np.log(ps), cmap="Oranges")#Dark2_r

        # We want to show all ticks...
        ax.set_xticks(np.arange(len(models)))
        ax.set_yticks(np.arange(len(models)))
        # ... and label them with the respective list entries
        ax.set_xticklabels(models)
        ax.set_yticklabels(models)

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        for i in range(len(models)):
            for j in range(len(models)):
                if ps[i, j] != np.float('nan'):
                    text = ax.text(j, i, f"{ps[i, j]:.2e}",
                                ha="center", va="center", color="w")
                    text = ax.text(j, i, f"{ps[i, j]:.2e}",
                                ha="center", va="center", color="black", alpha=.5)

        ax.set_title(f"P-value from t-test at k = {k}")
        fig.tight_layout()
        plt.savefig(independance_png.format(k))

if __name__ == "__main__":

    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    dataset_folder = "dataset/test/20x20"
    data_file = "tasks/SPECT/preprocessed/spect.pkl"
    implication_file = "tasks/SPECT/preprocessed/spect.canonical.pkl"
    results_csv = model_folder + "/attribute_clustering_results.csv"
    results_png = model_folder + "/attribute_clustering_results.png"
    independance_png = model_folder + "/attribute_clustering_independance_{}.png"
    FROM_CSV = False

    #train_a2v()
    emb_models_list = apply_boa()
    df = pd.read_csv(results_csv) if FROM_CSV else cluster_attributes(emb_models_list)
    plot(df)
    stats(df)
    #independence_tests(df)