import torch 

#closure objective
intents = torch.tensor([
    [0, 0, 0, 0],
    [1, 0, 0, 0],
    [0, 0, 1, 0],
    [0, 1, 0, 0],
    [1, 0, 1, 0],
    [0, 1, 0, 1],
    [0, 1, 1, 1],
    [1, 1, 1, 1]
])

def attribute_set_inclusion(x, refference):
    return (x <= refference).all().bool()

def close(x, intents):
    """Assume the intents are in the level order"""
    for intent in intents:
        if attribute_set_inclusion(x, intent):
            return intent


# expected result 0 1 1 0 --> 0 1 1 1
querry_1 = torch.tensor([0, 1, 1, 0])
print(close(querry_1, intents))

# expected result 0 0 0 1 --> 0 1 0 1
querry_2 = torch.tensor([0, 0, 0, 1])
print(close(querry_2, intents))

# expected result 1 0 0 1 --> 1 1 1 1
querry_3 = torch.tensor([1, 0, 0, 1])
print(close(querry_3, intents))

# expected result 0 1 0 1 --> 0 1 0 1
querry_4 = torch.tensor([0, 1, 0, 1])
print(close(querry_4, intents))