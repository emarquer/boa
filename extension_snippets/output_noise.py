import torch
distrib = torch.distributions.binomial.Binomial(probs=0.2)

x = torch.rand(8, 5, 4)
noise = lambda x: abs(x - distrib.sample(x.size()))


p = 0.5
k = torch.rand(50,50,20)
torch.clamp(k * 2 * p, 0, 1).mean()