import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch
from sklearn.metrics import roc_auc_score
import scipy.stats as stats
from sklearn.decomposition import PCA

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset as d
from boa_vae import *

#dataset.pad_2D(context, context.size(0) + test_padding[i], context.size(1) + test_padding[i])

ATTRIBUTE_LABELS = ["4 s.", "3 s.", "reg.", "iso."]
OBJECT_LABELS = ["square", "rectangle", "plain tri.", "iso. tri.", "equi. tri."]
USE_LABELS = True

def reconstruct(do_closure=True):
    # load model
    data = torch.load(boa_file, map_location='cpu')
    
    # load data
    # reset random seed
    torch.manual_seed(0)
    dataset = d.LatticeDataset("shapes")

    if do_closure:
        collate = CollateClosure(masks=False)
    else:
        collate = Collate(masks=False)
    
    dataloader = DataLoader(dataset,
                            batch_size=1,
                            shuffle=False,
                            num_workers=0,
                            collate_fn=collate)
      
    if do_closure:
        for contexts, intents, stops, links, sizes, sets_to_close, sets_closure in dataloader:
            context = contexts
            intent = intents
            sets_to_close, sets_closure = sets_to_close, sets_closure
    else:
        for contexts, intents, stops, links, sizes in dataloader:
            context = contexts
            intent = intents

    # reconstruct
    with torch.no_grad():
        if "v4" in model:
            mu = data['encoder'](context)
            o_embeddings = data['encoder'].encode_objects(context, mu)
            pred = data['decoder'](mu, o_embeddings)
        elif "v5" in model:
            a_embeddings, _, _ = data['encoder'](context)
            o_embeddings = data['encoder'].encode_objects(context, a_embeddings)
            pred = data['decoder'](a_embeddings, o_embeddings)
        else:
            a_embeddings, mu, logvar = data['encoder'](context)
            o_embeddings = data['encoder'].encode_objects(context, mu)
            pred = data['decoder'](mu, o_embeddings)
        if do_closure:
            if "v5" in model:
                pred_closure = data['closure_decoder'](a_embeddings, o_embeddings)
                to_close_emb = data['encoder'].encode_objects(sets_to_close, a_embeddings)
                closure_pred = data['closure_decoder'](a_embeddings, to_close_emb)
            else:
                pred_closure = data['closure_decoder'](mu, o_embeddings)
                to_close_emb = data['encoder'].encode_objects(sets_to_close, mu)
                closure_pred = data['closure_decoder'](mu, to_close_emb)
    
    # plot
    if do_closure:
        # plot closure too
        fig, axs = plt.subplots(2, 2, figsize=fig_size)

        axs[0, 0].imshow(contexts[0], vmin=0, vmax=1)
        axs[0, 0].set_title('Context')
        axs[0, 1].imshow(pred[0], vmin=0, vmax=1)
        axs[0, 1].set_title('Context prediction')
        axs[1, 0].imshow(sets_to_close[0], vmin=0, vmax=1)
        axs[1, 0].set_title('Sets to close')
        axs[1, 1].imshow(closure_pred[0], vmin=0, vmax=1)
        axs[1, 1].set_title('Closures')

    else:
        fig, axs = plt.subplots(1, 2, figsize=fig_size)

        axs[0].imshow(contexts[0], vmin=0, vmax=1)
        axs[0].set_title('Context')
        axs[1].imshow(pred[0], vmin=0, vmax=1)
        axs[1].set_title('Context prediction')

    plt.savefig(out_png)
    plt.cla()
    
    return a_embeddings[0] if "v5" in model else  mu[0], o_embeddings[0]
    
def plot_latent(a_emb, o_emb):
    pca = PCA(n_components=2)
    pca.fit(torch.cat((a_emb, o_emb,)))
    a_pca = pca.transform(a_emb)
    o_pca = pca.transform(o_emb)
    
    fig, ax = plt.subplots(figsize=fig_size)
    ax.scatter(a_pca[:,0], a_pca[:,1], marker='x', label="attributes")
    ax.scatter(o_pca[:,0], o_pca[:,1], marker='+', label="objects")
    ax.legend()

    if USE_LABELS:
        for i, a in enumerate(a_pca):
            ax.annotate(f" {ATTRIBUTE_LABELS[i]}", (a[0], a[1]), xytext=(0, 5), textcoords='offset pixels')
        for i, o in enumerate(o_pca):
            ax.annotate(f" {OBJECT_LABELS[i]}", (o[0], o[1]), xytext=(0, -5), textcoords='offset pixels')
    else:
        for i, a in enumerate(a_pca):
            ax.annotate(f" a {i}", (a[0] + 0.05, a[1] - 0.25), xytext=(0, 5), textcoords='offset pixels')
        for i, o in enumerate(o_pca):
            ax.annotate(f" o {i}", (o[0] + 0.05, o[1] + 0.05), xytext=(0, -5), textcoords='offset pixels')
    plt.savefig(out_latent_png)
    plt.cla()

if __name__ == "__main__":
    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    elif "v4" in model:
        from boa_v4 import *
    elif "v5" in model:
        from boa_v5 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    out_png = model_folder + "/shapes.png"
    out_latent_png = model_folder + "/shapes_latent.png"
    fig_size = (7,4)

    a_emb, o_emb = reconstruct()
    plot_latent(a_emb, o_emb)