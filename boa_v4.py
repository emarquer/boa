# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Bag of attributes
# 
# positional encoding
# no more rnn pre-encoder: replaced by attribute pre-encoder, negative/positive application, and random key
# alternative

# %%
import numpy as np
import torch
import torch.nn as nn
from boa_core import *
from collate_closure import CollateClosure
import typing as t
from math import exp
from datetime import datetime
time = lambda: datetime.now().strftime("%H:%M:%S.%f")[:-3]

def plot(encoder, decoder, closure_decoder, eval_dataloader):
    set_accuracies, set_row_accuracy, set_cell_accuracy = [], [], []
    set_aucs = []
    accuracies, row_accuracy, cell_accuracy = [], [], []
    aucs = []
    from sklearn.metrics import roc_auc_score
    with torch.no_grad():
        for j in range(1,10):
            print(f"{time()} --- {j/10} ---")
            for contexts, intents, stops, links, sizes, sets_to_close, sets_closure in eval_dataloader:
                contexts = contexts.to(device)
                intents = intents.to(device)
                sizes = sizes.to(device)
                sets_to_close = sets_to_close.to(device)
                sets_closure = sets_closure.to(device)
                
                a_emb = encoder(contexts)
                o_emb = encoder.encode_objects(contexts, a_emb)
                pred = decoder(a_emb, o_emb)
                
                to_close_emb = encoder.encode_objects(sets_to_close, a_emb)
                closure_pred = closure_decoder(a_emb, to_close_emb)

                for i in range(len(pred)):
                    try:
                        auc = roc_auc_score(contexts[i].view(-1).cpu(), pred[i].view(-1).cpu())
                        aucs.append(auc)
                    except ValueError:
                        pass

                for i in range(len(closure_pred)):
                    try:
                        auc = roc_auc_score(sets_closure[i].view(-1).cpu(), closure_pred[i].view(-1).cpu())
                        set_aucs.append(auc)
                    except ValueError:
                        pass

                accuracy = (contexts > 0) == (pred > j/10) # compare the cells
                cell_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)
                row_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)  # check for each sample in the batch if all its cells wwere correctly predicted
                accuracies.append(accuracy.cpu())
                
                accuracy = (sets_closure > 0) == (closure_pred > j/10) # compare the cells
                set_cell_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)
                set_row_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)  # check for each sample in the batch if all its cells wwere correctly predicted
                set_accuracies.append(accuracy.cpu())
            print("Reconstruction")
            print(f"accuracy (rate of contexts sets fully well-predicted): {torch.cat(accuracies, dim=0).type(torch.float).mean()}")
            print(f"row_accuracy: {torch.cat(row_accuracy).type(torch.float).mean()}")
            print(f"cell_accuracy: {torch.cat(cell_accuracy).type(torch.float).mean()}")
            print(f"AUC ROC average: {sum(aucs)/len(aucs):.5f}")
            print("Closure")
            print(f"accuracy (rate of contexts sets fully well-predicted): {torch.cat(set_accuracies, dim=0).type(torch.float).mean()}")
            print(f"row_accuracy: {torch.cat(set_row_accuracy).type(torch.float).mean()}")
            print(f"cell_accuracy: {torch.cat(set_cell_accuracy).type(torch.float).mean()}")
            print(f"AUC ROC average: {sum(set_aucs)/len(set_aucs):.5f}")
        
        print(f"{time()} plotting")
        import matplotlib.pyplot as plt
        for contexts, intents, stops, links, sizes, sets_to_close, sets_closure in eval_dataloader:
            a_emb = encoder(contexts.to(device))
            o_emb = encoder.encode_objects(contexts.to(device), a_emb)
            pred = decoder(a_emb, o_emb)

            f = plt.figure(figsize=(10, len(pred) * 2))
            for i, (prediction, sample) in enumerate(zip(pred.cpu(), contexts.cpu())):
                f.add_subplot(3, len(pred), i+1)
                plt.imshow(prediction.detach().cpu().numpy(), vmin=0, vmax=1)

                # f.add_subplot(3, len(pred), i+len(pred)+1)
                # plt.imshow(prediction.detach().round().cpu().numpy(), vmin=0, vmax=1)

                f.add_subplot(3, len(pred), i+len(pred)+len(pred)+1)
                plt.imshow(sample.cpu().numpy(), vmin=0, vmax=1)
            plt.savefig(name + '.png')
            break

class BOA_encoder_novar(BOA_encoder):
    def forward(self, input):
        """Compute the attribute embeddings from the context.
        
        Encode the attributes wrt. other attributes in each object.
        """
        batch, num_o, num_a = input.size()
        # input: [batch x num_o x num_a]
        
        # --- pre-embedding ---
        preencoded_o, _ = self.object_preencoder(input.view(batch * num_o, num_a, 1))
        # preencoded_o: [batch * num_o x num_a x 2 * emb_size]
        if self.reduction == 'max':
            preencoded_o = preencoded_o.view(batch, num_o, num_a, 2, self.pre_emb_size).max(dim=-2)[0]
        else:
            preencoded_o = preencoded_o.view(batch, num_o, num_a, 2, self.pre_emb_size).mean(dim=-2)
        preencoded_o = preencoded_o.view(batch, num_o, num_a, self.pre_emb_size)
        # preencoded_o: [batch x num_o x num_a x emb_size]
        
        
        # --- self-other encoder ---
        encoded_a_list = []
        for i in range(num_a):
            a = preencoded_o[:, :, i]
            not_a = preencoded_o[:, :, :i].sum(dim=-2) + preencoded_o[:, :, i+1:].sum(dim=-2)
            # a, not_a: [batch x num_o x emb_size], with a='self' and not_a='other'
            
            # --- aggregation over the objects ---
            o, (h_n, c_n) = self.attribute_encoder_rnn(torch.cat([a, not_a], dim = -1))
            if self.reduction == 'max':
                o_mean = o.view(batch, num_o, 2, self.emb_size).max(dim=-2)[0].max(dim=-2)[0]
            else:
                o_mean = o.view(batch, num_o, 2, self.emb_size).mean(dim=-2).mean(dim=-2)
            h_n = h_n.view(batch, -1)
            c_n = c_n.view(batch, -1)
            # o: [batch x emb_size]
            # h_n, c_n: [batch x 2 * layers * emb_size]
            encoded_a = self.attribute_encoder_linear(torch.cat([h_n, c_n, o_mean], dim=-1))
            # encoded_a: [batch x 2 x emb_size]
            encoded_a_list.append(encoded_a)
            
        a_embeddings = torch.stack(encoded_a_list, dim=-2)
        # a_embeddings: [batch x num_a x emb_size]

        # --- sampling (variational part of the model) ---
        a_embeddings = self.reparameterize(a_embeddings)

        return a_embeddings

    def reparameterize(self, h):
        mu, logvar = h.chunk(2, dim=-1)
        return mu

if __name__=="__main__":
    start_time = datetime.now()
    
    import sys
    mode_metric_learn = len(sys.argv) > 1 # metric learn if an argument is provided

    # --- hyperparameters ---
    batch_size = 8
    shuffle = True
    num_workers = 8
    lr = 1e-3
    weight_decay = 0
    epochs = 250
    device= 'cuda'
    
    # =: model itself :=
    name = "models/boa_v4"
    name_cointent = "models/boa_v4_c"
    emb_size = 128
    pre_emb_size = 64
    
    # =: metric learning :=
    att_cointent_slice = slice(0, emb_size//2)
    sizes_slice = slice(emb_size//2, emb_size//2 + emb_size//4)
    start_co_training = 0.5
    size_weight = lambda epoch_: min(epoch_ / (epochs / 2), 1)
    cointent_matrix_weight = 100
    att_cointent_weight = lambda epoch_: min(epoch_ / (epochs / 2), 1)# * 100
    
    # =: reconstruction noise :=
    noise_probability = 0.02
    distrib = torch.distributions.binomial.Binomial(probs=noise_probability)
    one_noise = lambda x: distrib.sample(x.size()).to(x.device)
    #continuous_noise = lambda x: torch.clamp(torch.rand_like(x) * 2 * noise_probability, 0, 1) # only works with p < 0.5
    noise = lambda x: (x - one_noise(x)).abs()
    
    
    # =: summary of all the parameters :=
    print(f"""Variant: V4 - not a VAE
    model file: {name + " -> " + name_cointent if mode_metric_learn else name}
    """)
    
    # --- dataset ---
    import os
    if os.path.isdir("latticenn/random-lattice-dataset/small_train/preprocessed"):
        train_dataset, dev_dataset = get_dataset("latticenn/random-lattice-dataset/small_train/preprocessed")
        eval_dataset = LatticeDataset("latticenn/random-lattice-dataset/small_test/preprocessed")

    if not mode_metric_learn:
        collate = CollateClosure(DataAugment(0.1, 0.01))
    else:
        collate = CollateClosure(DataAugment(0.1))

    # create dataloader
    train_dataloader = DataLoader(train_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    dev_dataloader = DataLoader(dev_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    eval_dataloader = DataLoader(eval_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)

    # --- initialize model ---
    # create or load model
    if not mode_metric_learn:
        encoder = BOA_encoder_novar(emb_size=emb_size, pre_emb_size=pre_emb_size, object_reduction="scaled mean").to(device)
        decoder = BOA_decoder(emb_size=emb_size).to(device)
        closure_decoder = BOA_decoder(emb_size=emb_size).to(device)
        data = {"encoder": encoder, "decoder": decoder, "closure_decoder": closure_decoder}
    else:
        data = torch.load(name + '.tch', map_location=device)
        encoder = data["encoder"]
        decoder = data["decoder"]
        closure_decoder = data["closure_decoder"]
        similarity_predictor = SimilarityPredictor(emb_size=emb_size//2).to(device)
        length_predictor = LengthPredictor(emb_size=emb_size//4).to(device)
        data["similarity_predictor"] = similarity_predictor
        data["length_predictor"] = length_predictor
        data["att_cointent_slice"] = att_cointent_slice
        data["sizes_slice"] = sizes_slice
        
        name = name_cointent

    print(f"{time()} starting metric learning" if mode_metric_learn else f"{time()} initial training")
    
    logs = []
    
    def dev_performance(epoch):
        with torch.no_grad():
            losses = []
            kls = []
            att_cointent_losses = []
            sizes_losses = []
            for contexts, intents, stops, links, sizes, sets_to_close, sets_closure in dev_dataloader:
                contexts = contexts.to(device) # [batch x num_o x num_a]
                intents = intents.to(device)
                sizes = sizes.to(device)
                sets_to_close = sets_to_close.to(device)
                sets_closure = sets_closure.to(device)

                a_emb = encoder(contexts)
                o_emb = encoder.encode_objects(contexts, a_emb)
                pred = decoder(a_emb, o_emb)
                
                to_close_emb = encoder.encode_objects(sets_to_close, a_emb)
                closure_pred = closure_decoder(a_emb, to_close_emb)

                loss = bce_criterion(pred, contexts.float())
                loss += bce_criterion(closure_pred, sets_closure.float())
                losses.append(loss.detach().cpu().item())

                if mode_metric_learn:
                    att_cointent_loss = mse_criterion(
                        cointent_matrix_weight * similarity_predictor(a_emb[:, :, att_cointent_slice]),
                        cointent_matrix_weight * attribute_co_intent_similarity(intents))
                    att_cointent_losses.append(att_cointent_loss.detach().cpu().item())
                    loss += att_cointent_loss * att_cointent_weight(epoch)
                
                    sizes_loss = mse_criterion(length_predictor(a_emb[:, :, sizes_slice]), sizes.float())
                    sizes_losses.append(sizes_loss.detach().cpu().item())
                    loss += sizes_loss * size_weight(epoch)
                    
            
            log = {"set": "dev", "epoch": epoch}
            log["reconstruction loss"] = sum(losses)/len(losses)
            if not mode_metric_learn:
                print(f"{time()} dev: {log['reconstruction loss']:9.5f}")
            else:
                message = f"{time()} dev: {log['reconstruction loss']:9.5f}"
                
                log["co-intent weight"] = att_cointent_weight(epoch)
                log["co-intent metric loss"] = sum(att_cointent_losses)/len(att_cointent_losses)
                message += f", co-intent loss: {log['co-intent metric loss']:11.5f} x {log['co-intent weight']:.2e}"
                
                log["size weight"] = size_weight(epoch)
                log["size metric loss"] = sum(sizes_losses)/len(sizes_losses)
                message += f", size loss: {log['size metric loss']:11.5f} x {log['size weight']:.2e}"
                    
                print(message)
            return log
        
    # training loop
    bce_criterion = nn.BCELoss()
    mse_criterion = nn.MSELoss()
    optim = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()], lr, weight_decay=weight_decay)
    
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optim, patience=20, verbose=True, eps=1e-7, factor=0.1, cooldown=20)
    
    if mode_metric_learn:
        optim_metric_similarity = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in similarity_predictor.parameters()], lr)
        optim_metric_length = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in length_predictor.parameters()], lr)
        optim_metric = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in length_predictor.parameters()] + [x for x in similarity_predictor.parameters()], lr)
    
    for epoch in range(epochs):
        losses = []
        att_cointent_losses = []
        sizes_losses = []
        for contexts, intents, stops, links, sizes, sets_to_close, sets_closure in train_dataloader:
            contexts = contexts.to(device)
            intents = intents.to(device)
            sizes = sizes.to(device)
            sets_to_close = sets_to_close.to(device)
            sets_closure = sets_closure.to(device)

            a_emb = encoder(contexts)
            o_emb = encoder.encode_objects(contexts, a_emb)
            pred = decoder(a_emb, o_emb)

            to_close_emb = encoder.encode_objects(sets_to_close, a_emb)
            closure_pred = closure_decoder(a_emb, to_close_emb)

            if torch.isnan(sets_closure).any() or torch.isinf(sets_closure).any():
                print("sets_closures", torch.isnan(sets_closure).any(), torch.isinf(sets_closure).any())
            if torch.isnan(closure_pred).any() or torch.isinf(closure_pred).any():
                print("closure_pred", torch.isnan(closure_pred).any(), torch.isinf(closure_pred).any())
            loss = bce_criterion(pred, contexts.float())
            loss += bce_criterion(closure_pred, sets_closure.float())
            losses.append(loss.detach().cpu().item())

            if mode_metric_learn:
                do_similarity = epoch % 2 == 0 or epoch > epochs * start_co_training
                do_length = epoch % 2 == 1 or epoch > epochs * start_co_training
                if do_similarity:
                    att_cointent_loss = mse_criterion(
                        cointent_matrix_weight * similarity_predictor(a_emb[:, :, att_cointent_slice]),
                        cointent_matrix_weight * attribute_co_intent_similarity(intents))
                    att_cointent_losses.append(att_cointent_loss.detach().cpu().item())
                    loss += att_cointent_loss * att_cointent_weight(epoch)
                if do_length:
                    sizes_loss = mse_criterion(length_predictor(a_emb[:, :, sizes_slice]), sizes.float())
                    sizes_losses.append(sizes_loss.detach().cpu().item())
                    loss += sizes_loss * size_weight(epoch)
                optim_metric.zero_grad()
                loss.backward()
                optim_metric.step()
            else:
                optim.zero_grad()
                loss.backward()
                optim.step()

        #if epoch > center: # only start after the annealing is in place
            #scheduler.step(sum(losses)/len(losses))
            
        if epoch%1 == 0 or epoch == epochs-1:
            log = {"set": "train", "epoch": epoch}
            log["reconstruction loss"] = sum(losses)/len(losses)
            if not mode_metric_learn:
                print(f"{time()} {epoch:3}: {log['reconstruction loss']:9.5f}")
            else:
                message = f"{time()} {epoch:3}: {log['reconstruction loss']:9.5f}"
                
                log["co-intent weight"] = att_cointent_weight(epoch)
                message += ", co-intent loss: "
                log["co-intent metric loss"] = sum(att_cointent_losses)/len(att_cointent_losses) if len(att_cointent_losses) > 0 else 'NaN'
                if log["co-intent metric loss"] != 'NaN':
                    message += f"{log['co-intent metric loss']:11.5f} x {log['co-intent weight']:.2e}"
                else:
                    message += " "*18
                
                log["size weight"] = size_weight(epoch)
                message += ", size loss: "
                log["size metric loss"] = sum(sizes_losses)/len(sizes_losses) if len(sizes_losses) > 0 else 'NaN'
                if log["size metric loss"] != 'NaN':
                    message += f"{log['size metric loss']:11.5f} x {log['size weight']:.2e}"
                else:
                    message += " "*18
                    
                print(message)
                
            logs.append(log)
            logs.append(dev_performance(epoch))
            data["epoch logs"] = logs
            torch.save(data, name + '.tch')
        
        if epoch == 0:
            plot(encoder, decoder, closure_decoder, eval_dataloader)

    torch.save(data, name + '.tch')


    # %%
    # --- evaluate final model ---
    plot(encoder, decoder, closure_decoder, eval_dataloader)
    print(f"{time()} done (total time = {str(datetime.now() - start_time)[:-3]})")
