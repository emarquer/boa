import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch
from sklearn.metrics import roc_auc_score
import scipy.stats as stats

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset as d
from boa_vae import *

def test():
    dataset = d.LatticeDataset(dataset_folder)

    # load model
    data = torch.load(boa_file, map_location='cpu')
    def reconstruct(context):
        a_embeddings, mu, logvar = data['encoder'](context.view(1, *context.size()))
        o_embeddings = data['encoder'].encode_objects(context.view(1, *context.size()), mu)
        pred = data['decoder'](mu, o_embeddings)
        return pred

    # run test
    records = []
    with torch.no_grad():
        for context, intents, sequence in dataset:
            context = torch.from_numpy(context).float()
            pred = reconstruct(context)

            try:
                auc = roc_auc_score(context.view(-1), pred.view(-1))
                records.append({'AUC ROC':auc, 'concept number': intents.shape[1], 'object number': context.size(0), 'attribute number': intents.shape[0]})
            except ValueError:
                pass

    # analyse and plot
    df = pd.DataFrame.from_records(records)

    df.to_csv(out_csv)
    return df

def plot_stats(df):

    def roundup(x, k):
        return int(round(x / float(k))) * k
    
    df['concept number rounded'] = df['concept number'].apply(lambda x: roundup(x-2, 50))
    plt.figure(figsize=fig_size)
    sns.scatterplot(data=df, y='AUC ROC', x='concept number', ci="sd", alpha=0.5, color='g')
    sns.lineplot(data=df, y='AUC ROC', x='concept number rounded', ci="sd")#, err_style='bars')
    sns.scatterplot(data=df, y='AUC ROC', x='concept number', ci="sd", alpha=0, color='g')

    plt.savefig(out_png)

    print(df.groupby('concept number rounded')['AUC ROC'].describe()[['mean', 'std']].to_markdown())

if __name__ == "__main__":
    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    out_png = model_folder + "/limits_concept_number.png"
    out_csv = model_folder + "/limits_concept_number.csv"
    dataset_folder = "dataset/test/20x20"
    fig_size = (7,4)
    FROM_CSV = False

    df = pd.read_csv(out_csv) if FROM_CSV else test()
    plot_stats(df)