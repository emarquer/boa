import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch
from sklearn.metrics import roc_auc_score
import scipy.stats as stats

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset
from boa_vae import *

test_densities = [
    i/10. for i in range(1,10)
]
samples_per_class = 100
size = (20, 20)

def test():
    # generate test data
    test_samples = {p: [
        np.random.choice(a=[True, False], size=size, p=[p, 1-p])
        for i in range(samples_per_class)
    ] for p in test_densities}
    #to check the overall density: sum(sum(sample.mean() for sample in test_samples[size])/len(test_samples[size])  for size in test_sizes)/len(test_sizes)

    # load model
    data = torch.load(boa_file, map_location='cpu')
    def reconstruct(context):
        a_embeddings, mu, logvar = data['encoder'](context.view(1, *context.size()))
        o_embeddings = data['encoder'].encode_objects(context.view(1, *context.size()), mu)
        pred = data['decoder'](mu, o_embeddings)
        return pred

    # run test
    aucs = {p: [] for p in test_densities}
    with torch.no_grad():
        for p in test_densities:
            print(p)
            for context in test_samples[p]:
                context = torch.from_numpy(context).float()
                pred = reconstruct(context)

                try:
                    auc = roc_auc_score(context.view(-1), pred.view(-1))
                    aucs[p].append(auc)
                except ValueError:
                    pass

    # analyse and plot
    records = [{'context density': p, 'AUC ROC': auc} for p in test_densities for auc in aucs[p] ]
    df = pd.DataFrame.from_records(records)

    df.to_csv(out_csv)
    return df

def plot_stats(df):
    plt.figure(figsize=fig_size)
    sns.barplot(data=df, y='AUC ROC', x='context density', ci="sd")

    plt.savefig(out_png)

    print(df.groupby('context density')['AUC ROC'].describe()[['mean', 'std']].to_markdown())

    results = []
    uniques = df['context density'].unique()
    for i, p1 in enumerate(uniques):
        for j, p2 in  enumerate(uniques):
            if j > i:
                p_value = stats.ttest_ind(
                    df[df['context density'] == p1]['AUC ROC'],
                    df[df['context density'] == p2]['AUC ROC']
                )[1]
                results.append((p1, p2, p_value,))
    print("p value > 0.01 (density 1, density 2, p value):", [(p1, p2, p_val) for p1, p2, p_val in results if p_val > 0.01])

if __name__ == "__main__":
    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    out_png = model_folder + "/limits_density.png"
    out_csv = model_folder + "/limits_density.csv"
    dataset_folder = "dataset/test/20x20"
    fig_size = (7,4)
    FROM_CSV = False

    df = pd.read_csv(out_csv) if FROM_CSV else test()
    plot_stats(df)