import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import torch
from sklearn.metrics import roc_auc_score
import scipy.stats as stats

import os,sys,inspect
current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir) 
import dataset
from boa_vae import *

test_padding = [
    0, 5, 10, 20, 50, 100
]
samples_per_class = 100
size = (20, 20)
p = 0.3

#dataset.pad_2D(context, context.size(0) + test_padding[i], context.size(1) + test_padding[i])

def test():
    # generate test data
    test_samples = {pad: [
        np.random.choice(a=[True, False], size=size, p=[p, 1-p])
        for i in range(samples_per_class)
    ] for pad in test_padding}
    #to check the overall density: sum(sum(sample.mean() for sample in test_samples[size])/len(test_samples[size])  for size in test_sizes)/len(test_sizes)

    # load model
    data = torch.load(boa_file, map_location='cpu')
    def reconstruct(context):
        a_embeddings, mu, logvar = data['encoder'](context.view(1, *context.size()))
        o_embeddings = data['encoder'].encode_objects(context.view(1, *context.size()), mu)
        pred = data['decoder'](mu, o_embeddings)
        return pred

    # run test
    aucs = {pad: [] for pad in test_padding}
    with torch.no_grad():
        for pad in test_padding:
            print(pad)
            for context in test_samples[pad]:
                context = torch.from_numpy(context).float()
                context = dataset.pad_2D(context, context.size(0) + pad, context.size(1) + pad)
                pred = reconstruct(context)

                try:
                    auc = roc_auc_score(context.view(-1), pred.view(-1))
                    aucs[pad].append(auc)
                except ValueError:
                    pass

    # analyse and plot
    records = [{'context padding': pad, 'AUC ROC': auc} for pad in test_padding for auc in aucs[pad] ]
    df = pd.DataFrame.from_records(records)

    df.to_csv(out_csv)
    return df
    
def plot_stat(df):
    plt.figure(figsize=fig_size)
    sns.barplot(data=df, y='AUC ROC', x='context padding', ci="sd")

    plt.savefig(out_png)

    print(df.groupby('context padding')['AUC ROC'].describe()[['mean', 'std']].to_markdown())

if __name__ == "__main__":
    import sys
    import os
    model = "boa"# boa_retrain_cointent # boa_v0_c
    if len(sys.argv) > 1:
        model = sys.argv[1]
    if "v0" in model:
        from boa_v0 import *
    elif "v3" in model:
        from boa_v3 import *
    
    boa_file = f"models/{model}.tch"
    model_folder = f"outputs/{model}"
    if not os.path.exists(model_folder):
        os.makedirs(model_folder)
    out_png = model_folder + "/limits_padding.png"
    out_csv = model_folder + "/limits_padding.csv"
    fig_size = (7,4)
    FROM_CSV = False

    df = pd.read_csv(out_csv) if FROM_CSV else test()
    plot_stat(df)