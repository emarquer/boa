import torch

def attribute_co_intent_similarity(intents: torch.Tensor, epsilon=1e-8) -> torch.Tensor:
    """ Computes the pairwise co-intent similarity matrix for intents.

    The co-intent similarity between 2 attributes i and j is defined as:
        1 if neither i nor j occur in the intent matrix,
        (2 * #intents containing both i and j) / (#intents containing i + #intents containing j + epsilon) otherwise.
    
    :param intents:
        FloatTensor or LongTensor of intents.
        Must be either of shape (#intents, #attributes) or (batch, #intents, #attributes).
    :param epsilon:
        Small value to handle attributes never occuring in the intents (padding attributes typically).
    """

    # co_occurence_matrix[i,j] = number of time i & j appear together in the intents
    co_occurence_matrix = torch.matmul(intents.transpose(-1, -2), intents)

    # occurence_vector[i] = number of time i appears in the intents
    occurence_vector = intents.sum(dim=-2)

    # appearance_matrix[i,j] = #i + #j + epsilon
    if intents.dim() == 3: # batch variant
        occurence_matrix = occurence_vector.expand(co_occurence_matrix.size(-1), *occurence_vector.size()).transpose(1, 0)
        appearance_matrix = occurence_matrix.transpose(-1, -2) + occurence_matrix + epsilon
    else:
        appearance_matrix = occurence_vector.expand(co_occurence_matrix.size()).transpose(-1, -2) + occurence_vector + epsilon

    # co-occurence similarity between i and j:
    # 2 * #intents containing both i&j / (#intents containing i + #intents containing j + epsilon)
    # (2 * co_occurence_matrix[i,j].float()) / (intents[:,i].sum() + intents[:,j].sum() + epsilon).float()
    co_occurence_similarity_matrix = 2 * co_occurence_matrix.float() / appearance_matrix

    # if the sum of the individual occurences are 0, it means that both attributes are empty, the similarity is 1
    co_occurence_similarity_matrix += (appearance_matrix <= epsilon).float() 

    return co_occurence_similarity_matrix