import torch

def kl_divergence(mu, logvar):
    """ Computes the Kullback-Leibler divergence for Variational Auto-Encoders.

    See Appendix B from VAE paper:
        Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
        https://arxiv.org/abs/1312.6114
        0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)

    :param mu:
        Mean of the predicted distribution. Must be of the same size as logvar.
    :param logvar:
        Logarithm of the variance of the predicted distribution. Must be of the same size as mu.
    :return:
        Kullback-Leibler divergence.
    """
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
    return KLD